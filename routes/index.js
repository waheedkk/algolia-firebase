var express = require('express');
var router = express.Router();
var firebase = require('firebase');
var algoliasearch = require('algoliasearch');
var client = algoliasearch('DYH23ANMQA', '563cc4d67dda2db2648c9a5f1799568b');
var index = client.initIndex('Event');

var serviceAccount = require('../common/MemoryApp-5d406fb32bfa.json');

// Connect to our Firebase contacts data
var fb = firebase.initializeApp({
  serviceAccount: serviceAccount,
  databaseURL: "https://memoryapp-20e33.firebaseio.com/"
});

/* GET home page. */
router.get('/', function(req, res, next) {

  function initIndex(dataSnapshot) {
    // Array of data to index
    var objectsToIndex = [];

    // Get all objects
    var values = dataSnapshot.val();

    // Process each Firebase ojbect
    for (var key in values) {
      if (values.hasOwnProperty(key)) {
        // Get current Firebase object
        var firebaseObject = values[key];

        // Specify Algolia's objectID using the Firebase object key
        firebaseObject.objectID = key;

        // Add object for indexing
        objectsToIndex.push(firebaseObject);
      }
    }

    // Add or update new objects
    index.saveObjects(objectsToIndex, function(err, content) {
      if (err) {
        throw err;
      }

      console.log('Firebase<>Algolia import done: ', content);
    });
  }

  var db = fb.database();
  var ref = db.ref("Event");
  ref.once("value", function(snapshot) {
    console.log("snapshot: ", snapshot.val());
    initIndex(snapshot);
  });
  
});

module.exports = router;
